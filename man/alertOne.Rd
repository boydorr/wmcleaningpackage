% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/oneError.R
\name{alertOne}
\alias{alertOne}
\title{alerts on one error type}
\usage{
alertOne(data.errors, condition, error.string)
}
\arguments{
\item{data.errors}{list(data, errors) - list consisting of data.frame data and data.frame errors}

\item{condition}{a condition that should evaluate to true if an error is found}

\item{error.string}{the string to add to the errors dataframe if an error is found}
}
\value{
returns data.errors, with data part unchanged, and errors added to existing list
}
\description{
Identifies a specified error and adds information to log; does not correct the error
}
